locals {
  namespace        = "${var.application}-${var.environment}"
  application_name = "${var.application}-${var.environment}"
}
resource "kubernetes_namespace_v1" "this" {
  metadata {
    annotations = {
      name = local.namespace
    }
    labels = var.labels

    name = local.namespace
  }
}
resource "kubernetes_service_account_v1" "this" {
  metadata {
    name      = local.application_name
    namespace = local.namespace
    labels    = var.labels
  }
  secret {
    name = local.application_name
  }

  depends_on = [kubernetes_namespace_v1.this]
}
resource "kubernetes_secret_v1" "this" {
  metadata {
    name      = local.application_name
    namespace = local.namespace
    annotations = {
      "kubernetes.io/service-account.name" = local.application_name
    }
    labels = var.labels
  }

  type = "kubernetes.io/service-account-token"

  depends_on = [kubernetes_namespace_v1.this]
}
resource "kubernetes_role_v1" "this" {
  metadata {
    name      = local.application_name
    namespace = local.namespace
    labels    = var.labels
  }
  rule {
    api_groups = ["*"]
    resources  = ["*"]
    verbs      = ["*"]
  }

  depends_on = [kubernetes_namespace_v1.this]
}
resource "kubernetes_role_binding_v1" "this" {
  metadata {
    name      = local.application_name
    namespace = local.namespace
    labels    = var.labels
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = local.application_name
  }
  subject {
    kind      = "ServiceAccount"
    name      = local.application_name
    namespace = local.namespace
  }

  depends_on = [kubernetes_role_v1.this, kubernetes_service_account_v1.this]
}
resource "helm_release" "this" {
  name       = "ga-${local.application_name}"
  chart      = "gitlab-agent"
  repository = "https://charts.gitlab.io"
  version    = var.gitlab_agent_version
  namespace  = local.namespace
  set {
    name  = "replicas"
    value = 1
  }
  set {
    name  = "config.token"
    value = var.gitlab_agent_token
  }
    set {
    name  = "config.kasAddress"
    value = var.gitlab_kas_address
  }
  set {
    name  = "serviceAccount.create"
    value = false
  }
  set {
    name  = "serviceAccount.create"
    value = false
  }
  set {
    name  = "serviceAccount.name"
    value = local.application_name
  }
  set {
    name  = "rbac.create"
    value = false
  }
  set {
    name  = "resources.requests.cpu"
    value = "50m"
  }
  set {
    name  = "resources.requests.memory"
    value = "64Mi"
  }
  set {
    name  = "resources.limits.cpu"
    value = "250m"
  }
  set {
    name  = "resources.limits.memory"
    value = "512Mi"
  }

  depends_on = [kubernetes_role_binding_v1.this]
}
