variable "environment" {
  description = "The environment to deploy to"
  type        = string
}
variable "application" {
  description = "The application to deploy, is the name of the service account and namespace in combination with the environment name"
  type        = string
}
variable "labels" {
  description = "The labels to apply to the deployment"
  type        = map(string)
  default = {
    Owner       = "Terraform"
    Environment = "Development"
  }
}
variable "gitlab_agent_version" {
  description = "The version of the gitlab agent to deploy"
  type        = string
  default     = "1.20.0"
}
variable "gitlab_agent_token" {
  description = "The token to use for the gitlab agent"
  type        = string
}
variable "gitlab_kas_address" {
  description = "The KAS Address of the Gitlab instance"
  type        = string
  default     = "wss://kas.gitlab.com"
}